import Vue from 'vue'

import LoadingIcon from '~/components/common/loading-icon'
Vue.component('LoadingIcon', LoadingIcon)


import CardPetInfo from '~/components/card-pet-info'
Vue.component('CardPetInfo', CardPetInfo)

import CardSeasonInfo from '~/components/card-season-info'
Vue.component('CardSeasonInfo', CardSeasonInfo)

import CopangComp from '~/components/copang-comp'
Vue.component('CopangComp', CopangComp)
